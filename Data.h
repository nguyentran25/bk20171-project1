#ifndef DATA_H
#define DATA_H

using namespace std;

class Data
{
public:
	string data_from_keyboard(); //Nhap du lieu tu ban phim
	string data_from_file(); // Nhap du lieu tu file, ten file nhap tu ban phim
	string create_data_random(bool text_or_bit); // Tao du lieu ngau nhien
	bool create_char_freq_table();
	void write_to_file();
	void view_data();
	string get_text();
	string get_string_bit();
	map<char, int> get_char_freq_table();
	void set_text(string text);
	void set_string_bit(string string_bit);

private:
	string text; // Luu tru noi dung van ban nhap vao
	string string_bit; // Luu tru chuoi nhi phan duoc nhap vao
	map<char, int> char_freq_table; // Luu tru tan so cua tung ki tu trong van ban
};

#endif