#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include "Random.h"

using namespace std;

const char RandomData::text_char[] =
	"0123456789"
	"`~!@#$%^&*()-_=+{[]};:'/?>.<,\" "
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz";

const char RandomData::bit_char[] = "01";

string RandomData::random_string_text()
{
	unsigned int size;
	string string_text;
	int string_length = sizeof(text_char) - 1;
	cout << "Nhap kich thuoc file (KB): ";
	cin >> size;
	for(unsigned int i = 0; i < size*1024; i++)
	{
		string_text += text_char[rand() % string_length];
	}
	cout << "Da tao xong du lieu ngau nhien";
	return string_text;
}

string RandomData::random_string_bit()
{
	unsigned int size;
	string string_bit;
	int string_length = sizeof(bit_char) - 1;
	cout << "Nhap kich thuoc file (KB): ";
	cin >> size;
	for(unsigned int i = 0; i < size*1024; i++)
	{
		string_bit += bit_char[rand() % string_length];
	}
	cout << "Da tao xong du lieu ngau nhien";
	return string_bit;
}