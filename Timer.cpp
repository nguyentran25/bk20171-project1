#include <cstddef>
#include "Timer.h"

using namespace std;

Timer::Timer()
{
	gettimeofday(&_start_t,NULL);	
}

Timer::~Timer(){
}

double Timer::getElapsedTime()
{
	timeval t;
	gettimeofday(&t,NULL);
	
	double	elapsedtime = (t.tv_sec - _start_t.tv_sec) * 1000.0;
	elapsedtime += (t.tv_usec - _start_t.tv_usec) / 1000.0;
	
	elapsedtime = elapsedtime*0.001;
	
	return elapsedtime;
}