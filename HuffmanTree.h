#ifndef HuffmanTree_H
#define HuffmanTree_H

using namespace std;

class HuffmanTree
{
public:	
	struct CharBit // Cau truc de luu ki tu va chuoi nhi phan tuong ung
	{
		char data;
    	string bit;
	};
	struct MinHeapNode // Node cua cay Huffman, bao gom thong tin ve ki tu, tan so, co 2 con tro left va right
	{
    	char data;
    	unsigned freq;
    	MinHeapNode *left, *right;
 	
    	MinHeapNode(char data, unsigned freq)
    	{
        	left = right = NULL;
        	this->data = data;
        	this->freq = freq;
    	}
	};

	struct compare // Phep so sanh su dung trong hang doi uu tien
	{
    	bool operator()(MinHeapNode* l, MinHeapNode* r)
    	{
	        return (l->freq > r->freq);
    	}
	};

	void build_huffman_tree(map<char,int> M);
	void create_char2bit_table(MinHeapNode* root, string str);
	void encode(string text);
	void decode(string bit);
	void write_to_file();
	vector<CharBit> get_char2bit_table();
	string get_text_decoded();
	string get_bit_encoded();
	MinHeapNode* get_tree_root();
	void set_char2bit_table_empty();
	void set_view_mediate(bool view_mediate);

private:
	MinHeapNode* tree_root; // Goc cua cay Huffman
	vector<CharBit> char2bit_table; // Luu tru tap cac ki tu va chuoi nhi phan tuong ung
	string bit_encoded; // Chuoi nhi phan thu duoc sau khi ma hoa van ban
	string text_decoded; // Doan van ban nhan duoc sau khi ma hoa chuoi nhi phan
	bool view_mediate;
};

#endif