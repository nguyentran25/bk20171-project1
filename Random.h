#ifndef RANDOM_H
#define RANDOM_H

using namespace std;

class RandomData
{
public:
	string random_string_text();
	string random_string_bit();

private:
	static const char text_char[];
	static const char bit_char[];
};

#endif