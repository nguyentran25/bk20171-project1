#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

using namespace std;

class Timer{
public:
	timeval	_start_t;
	Timer();
	~Timer();
	double getElapsedTime();	
};

#endif