all: Main

Main: Main.o Data.o HuffmanTree.o Menu.cpp Timer.o Random.o
	g++ Main.cpp Data.cpp HuffmanTree.cpp Menu.cpp Timer.cpp Random.cpp -o main -O2 -Wall

clean:
	rm $(all) *.o