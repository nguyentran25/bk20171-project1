#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <iomanip>
#include "Data.h"
#include "HuffmanTree.h"
#include "Timer.h"
#include "Menu.h"


using namespace std;

bool view_mediate = false;
bool view_analysis = false;

void Menu::input_keyboard()
{
	cout << "*Nhap du lieu tu ban phim*" << endl;
	cout << "Nhap 1 de nhap van ban, 2 de nhap chuoi nhi phan" << endl;
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		cout << "Nhap van ban tu ban phim, nhap '-1' o cuoi va enter de hoan thanh" << endl;
		data.set_text(data.data_from_keyboard());
	}
	else if (ch == '2')
	{
		cout << "Nhap chuoi nhi phan tu ban phim, nhap '-1' o cuoi va enter de hoan thanh" << endl;
		data.set_string_bit(data.data_from_keyboard());
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}

void Menu::input_file()
{
	cout << "*Nhap du lieu tu file*" << endl;
	cout << "Nhap 1 de nhap van ban, 2 de nhap chuoi nhi phan" << endl;
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		cout << "Nhap van ban tu file" << endl;
		data.set_text(data.data_from_file());
	}
	else if (ch == '2')
	{
		cout << "Nhap chuoi nhi phan tu file" << endl;
		data.set_string_bit(data.data_from_file());
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}

void Menu::input_random()

{
	cout << "*Tao du lieu ngau nhien*" << endl;
	cout << "Nhap 1 de tao du lieu ngau nhien dang van ban, "
			"2 de tao du lieu ngau nhien dang chuoi nhi phan" << endl;
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		cout << "Tao du lieu ngau nhien dang van ban" << endl;
		data.set_text(data.create_data_random(true));
	}
	else if (ch == '2')
	{
		cout << "Tao du lieu ngau nhien dang chuoi nhi phan" << endl;
		data.set_string_bit(data.create_data_random(false));
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}

void Menu::review_data()
{
	cout << "*Xem lai du lieu*" << endl;
	data.view_data();
}

void Menu::write_input_data_to_file()
{
	cout << "*Ghi du lieu ra file*" << endl;
	data.write_to_file();
}

void Menu::build_huffman()
{
	Timer time;
	cout << "*Xay dung cay Huffman*" << endl;
	if( !data.create_char_freq_table() )
	{
		cout << "Chua co du lieu text de xay dung cay HuffMan" << endl << endl;
		return;
	}
	map<char, int> M = data.get_char_freq_table();
	if (view_mediate == true)
	{
		map<char,int>::iterator it;
		cout << "B1: Xay dung bang tan so cho cac ki tu: " << endl;
		for (it = M.begin(); it != M.end(); ++it)
		{
			cout << it->first << ": " << it->second;
			if (it->first == '\n')
				cout << " (Ki tu xuong dong)" << endl;
			else if (it->first == ' ')
				cout << " (Ki tu space)" << endl;
			else cout << endl;
		}
	} //Xem ket qua trung gian
	huffman_tree.set_char2bit_table_empty();
	huffman_tree.set_view_mediate(view_mediate);
	huffman_tree.build_huffman_tree(M);
	HuffmanTree::MinHeapNode* h = huffman_tree.get_tree_root();
	huffman_tree.create_char2bit_table(h, "");
	if(view_mediate == true)
	{
		cout << "B4: Xay dung bang ma hoa ki tu thanh chuoi bit:" << endl;
		vector <HuffmanTree::CharBit> char2bit_table;
		char2bit_table = huffman_tree.get_char2bit_table();
		vector<HuffmanTree::CharBit>::iterator it;
		for(it = char2bit_table.begin(); it != char2bit_table.end(); ++it)
			cout << (*it).data << ": " << (*it).bit << endl << endl;
	} // Xem ket qua trung gian
	cout << "Da xay dung xong cay Huffman" << endl;
	if(view_analysis == true)
	{
		cout << setprecision(4) << fixed << "Kich thuoc du lieu la: "
		<< data.get_text().size()/(1024.0) << "KB" << endl;
		cout << "Thoi gian xay dung cay Huffman la: " << time.getElapsedTime() << endl << endl;
	}
}

void Menu::encode_decode()
{
	cout << "*Encode/Decode*" << endl;
	cout << "Nhap 1 de Encode, 2 de Decode" << endl;
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		cout << "Ma hoa doan van ban thanh chuoi nhi phan" << endl;
		encode();
	}
	else if (ch == '2')
	{
		cout << "Giai ma chuoi nhi phan thanh doan van ban" << endl;
		decode();
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;

}

void Menu::encode()
{
	Timer time;
	cout << "*Encode*" << endl;
	string tmp = data.get_text();
	if(tmp == "")
	{
		cout << "Chua co du lieu" << endl << endl;
		return;
	}
	huffman_tree.encode(tmp);
	cout << "Da ma hoa xong" << endl;
	if(view_analysis == true)
	{
		cout << setprecision(4) << fixed << "Kich thuoc du lieu truoc khi ma hoa la: "
		<< data.get_text().size()/(1024.0) << "KB" << endl;
		cout << setprecision(4) << fixed << "Kich thuoc du lieu sau khi ma hoa la: "
		<< huffman_tree.get_bit_encoded().size()/(8*1024.0) << "KB" << endl;
		cout << "Thoi gian ma hoa la: " << time.getElapsedTime() << endl << endl;
	}
}

void Menu::decode()
{
	Timer time;
	cout << "*Decode*" << endl;
	string tmp2 = data.get_string_bit();
	if(tmp2 == "")
	{
		cout << "Chua co du lieu" << endl << endl;
		return;
	}
	huffman_tree.decode(tmp2);
	cout << "Da giai ma xong" << endl;
	if(view_analysis == true)
	{
		cout << setprecision(4) << fixed << "Kich thuoc du lieu truoc khi giai ma la: "
		<< data.get_string_bit().size()/(8*1024.0) << "KB" << endl;
		cout << setprecision(4) << fixed << "Kich thuoc du lieu sau khi giai ma la: "
		<< huffman_tree.get_text_decoded().size()/(1024.0) << "KB" << endl;
		cout << "Thoi gian giai ma la: " << time.getElapsedTime() << endl << endl;
	}
}

void Menu::view_result()
{
	cout << "*Xem ket qua tren man hinh*" << endl;
	cout << "Nhap 1 de xem chuoi nhi phan sau khi ma hoa van ban, "
			"2 de xem doan van ban thu duoc sau khi giai ma chuoi nhi phan" << endl;
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		// cout << "Chuoi nhi phan thu duoc" << endl;
		string result = huffman_tree.get_bit_encoded();
		if (result == "")
		{
			cout << "Chua co du lieu" << endl << endl;
			return;
		}
		cout << result << endl;
	}
	else if (ch == '2')
	{
		// cout << "Doan van ban" << endl;
		string result = huffman_tree.get_text_decoded();
		if (result == "")
		{
			cout << "Chua co du lieu" << endl << endl;
			return;
		}
		cout << result << endl;
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}

void Menu::write_result_to_file()
{
	cout << "*Ghi ket qua ra file*" << endl;
	huffman_tree.write_to_file();
}

void Menu::mediate()
{
	cout << "*Xem/Khong xem ket qua trung gian khi tinh toan*" << endl;
	cout << "Go 1 de xem, 2 de khong xem: ";
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		view_mediate = true;
		cout << "Da chon xem ket qua trung gian" << endl << endl;
	}
	else if (ch == '2')
	{
		view_mediate = false;
		cout << "Da chon khong xem ket qua trung gian" << endl << endl;
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}

void Menu::analysis()
{
	cout << "*Xem/Khong xem chuc nang thong ke tinh toan*" << endl;
	cout << "Go 1 de xem, 2 de khong xem: ";
	char ch;
	cin >> ch;
	if (ch == '1')
	{
		view_analysis = true;
		cout << "Da chon xem chuc nang thong ke tinh toan" << endl << endl;
	}
	else if (ch == '2')
	{
		view_analysis = false;
		cout << "Da chon khong xem chuc nang thong ke tinh toan" << endl << endl;
	}
	else cout << "Nhap sai, quay lai menu chinh" << endl << endl;
}