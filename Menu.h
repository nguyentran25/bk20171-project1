#ifndef Menu_H
#define Menu_H
#include "Data.h"
#include "HuffmanTree.h"

using namespace std;


class Menu
{
public:
	void input_keyboard();
	void input_file();
	void input_random();
	void review_data();
	void write_input_data_to_file();
	void build_huffman();
	void encode_decode();
	void encode();
	void decode();
	void view_result();
	void write_result_to_file();
	void analysis();
	void mediate();
	
private:
	bool view_mediate;
	bool view_analysis;
	Data data; // Khoi tao doi tuong data de luu tru du lieu
	HuffmanTree huffman_tree; /* Khoi tao doi tuong huffman_tree de thuc hien viec xay dung
	cay huffman, encode va decode */

};

#endif