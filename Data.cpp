#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include "Data.h"
#include "Random.h"

using namespace std;

string Data::data_from_keyboard(){
	string str;
	string line;
	while (getline(cin, line)) {
    	if (line == "-1")
    		break;
    	str += line + '\n';
    }
    cout << "Da nhap xong du lieu tu ban phim";
    return str;
}

string Data::data_from_file(){
	string str;
 	string file_name;
	cout << "Nhap ten file: ";
	cin >> file_name;
 	ifstream file (file_name.c_str());
 	while(!file.eof()){
 		string line;
 		getline(file,line);
 		str += line + "\n";
 	}
 	file.close();
 	cout << "Da nhap xong du lieu tu file";
 	return str;
}

string Data::create_data_random(bool bit_or_text){
	RandomData random_data;
	if(!bit_or_text)
		return random_data.random_string_bit();	
	return random_data.random_string_text();
}

void Data::view_data(){
	char c;
	cout << "Nhap 1 de xem du lieu text, nhap 2 de xem du lieu chuoi bit: ";
	cin >> c;
	if(c == '1'){
		if (this->text == "" || this->text == "\n"){
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	cout << "\n======================\n";
		cout << this->text;
	}
	else if(c == '2'){
		if (this->string_bit == ""){
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	cout << "\n======================\n";
		cout << this->string_bit;
	}
	else cout << "Nhap sai, quay lai menu truoc";
}

bool Data::create_char_freq_table(){
	string str = this->text;
	if (str == "" || str == "\n"){
		return false;
	}
	map<char, int> char_freq_table;
	for(unsigned int i= 0; i<str.size(); i++){
		char s = str[i];
		int freq = char_freq_table[s];
		freq++;
		char_freq_table[s] = freq;
	}
	this->char_freq_table = char_freq_table;
	return true;
}

string Data::get_text(){
	return this->text;
}

string Data::get_string_bit(){
	return this->string_bit;
}

void Data::set_text(string text){
	this->text = text;
}

void Data::set_string_bit(string string_bit){
	this->string_bit = string_bit;
}

void Data::write_to_file(){
	string file_name;
	char c;
	cout << "Nhap 1 de ghi du lieu text, nhap 2 de ghi du lieu chuoi bit: ";
	cin >> c;
	if (c != '1' && c != '2')
	{
		cout<<"Nhap sai, quay lai menu truoc" << endl;
		return;
	}
	cout << endl << "Nhap ten file: ";
	cin >> file_name;
	fstream f;
    if (c == '1'){
    	if (this->text == ""){
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	f.open(file_name.c_str(), ios::out);
		f << this->text;
        cout << "Da ghi thanh cong" << endl;
    	f.close();
    	return;
    }
    if (c == '2'){
    	if (this->string_bit == ""){
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	f.open(file_name.c_str(), ios::out);
		f << this->string_bit;
		cout << "Da ghi thanh cong" << endl;
    	f.close();
    	return;
    }
}

map<char, int> Data::get_char_freq_table(){
	return this->char_freq_table;
}