#include <iostream>
#include <map>
#include <vector>
#include "Menu.h"

using namespace std;

int main(){
	Menu Menu;
	while(true)
	{
		bool exit = false;
		short c;
		cout << "\n\n======================\n";
		cout << "CHUONG TRINH MA HOA VAN BAN BANG PHUONG PHAP HUFFMAN CODING\n";
		cout << "======================\n";
		cout << " 1. Nhap du lieu tu ban phim\n";
		cout << " 2. Nhap du lieu tu file (ten nhap tu ban phim)\n";
		cout << " 3. Tao du lieu ngau nhien\n";
		cout << " 4. Xem du lieu tren man hinh\n";
		cout << " 5. Ghi du lieu ra file (ten nhap tu ban phim)\n";
		cout << " 6. Thuc hien tinh toan (xay dung cay huffman)\n";
		cout << " 7. Encode/Decode\n";
		cout << " 8. Xem ket qua tren man hinh\n";
		cout << " 9. Ghi ket qua ra file\n";
		cout << " 10. Xem/Khong xem ket qua trung gian\n";
		cout << " 0. Thoat khoi chuong trinh\n";
		cout << "----------------------\n";
		cout << "Chon chuc nang: ";
		cin >> c;
		cout << "\n\n";
		switch(c){
			case 1:{
				Menu.input_keyboard();
				break;
			}

			case 2:{
				Menu.input_file();
				break;
			}

			case 3:{
				Menu.input_random();
				break;
			}

			case 4:{
				Menu.review_data();
				break;
			}

			case 5:{
				Menu.write_input_data_to_file();
				break;
			}

			case 6:{
				Menu.build_huffman();
				break;
			}

			case 7:{
				Menu.encode_decode();
				break;
			}

			case 8:{
				Menu.view_result();
				break;
			}

			case 9:{
				Menu.write_result_to_file();
				break;
			}

			case 10:{
				Menu.analysis();
				break;
			}

			case 0:{
				exit = true;
				break;
			}

			default: cout << "Nhap sai, moi nhap lai" << endl;
		} //end switch main menu
		if (exit == true) break;
	} // end while main
	cout << "Ket thuc chuong trinh" << endl;
	return 0;
}