#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <queue>
#include "HuffmanTree.h"

using namespace std;

void HuffmanTree::build_huffman_tree(map<char,int> M)
{
	priority_queue<MinHeapNode*, vector<MinHeapNode*>, compare> minHeap;
	MinHeapNode *left, *right, *top;
 	map<char,int>::iterator it;

    if(view_mediate == true)
    {
    	cout << endl 
        << "B2: Tao min heap va them tat ca cac phan tu vao, do uu tien la tan so:" << endl;
    }

    for (it = M.begin(); it != M.end(); ++it){
    	MinHeapNode* h = new MinHeapNode(it->first, it->second);
    	if(view_mediate == true)
    	{
    		cout << "Them node chua ki tu: '" << it->first 
            << "' va co tan so: " << it->second << endl;
    		if( ++it == M.end())
        	cout << "Da them tat ca cac node vao min heap" << endl << endl;
        	it--;
    	}
        minHeap.push(h);
    }

    if(view_mediate == true)
    {
    	cout << "B3: Lay 2 phan tu co tan so nho nhat ra khoi min heap ghep voi nhau tao "
        "thanh 1 node moi, sau do them node nay vao min heap, lap lai "
        "den khi min heap chi con 1 phan tu:" << endl;
    }
    while (minHeap.size() != 1)
    {
        left = minHeap.top();
        minHeap.pop();
 
        right = minHeap.top();
        minHeap.pop();
 
        top = new MinHeapNode('$', left->freq + right->freq);
        top->left = left;
        top->right = right;
        minHeap.push(top);
        if(view_mediate == true)
    	{
    		cout << "Node trai co tan so la: " << left->freq << endl;
    		cout << "Node phai co tan so la: " << right->freq << endl;
    		cout << "Tao node moi co tan so la tong tan so node trai va node phai: " 
            << left->freq + right->freq << endl;
    		if (minHeap.size() == 1)
    		cout << "Da xay dung xong cay huffman, co goc la "
            "phan tu duy nhat con lai trong min heap" << endl << endl;
    	}
    }
    this->tree_root = minHeap.top();
}

void HuffmanTree::create_char2bit_table(HuffmanTree::MinHeapNode* root, string str)
{
	vector<CharBit> char2bit_table;
    if (!root)
    {
        return;
    }
    if(root->data != '$')
    {
        HuffmanTree::CharBit cbit;
        cbit.data = root->data;
        cbit.bit = str;
        this->char2bit_table.push_back(cbit);
    }
    create_char2bit_table(root->left, str + "0");
    create_char2bit_table(root->right, str + "1");
}

void HuffmanTree::encode(string text)
{
	if(this->char2bit_table.size() == 0){
		cout << "Chua xay dung cay Huffman" << endl;
		return;
	}
	string string_bit;
	for(unsigned int i = 0; i < text.size(); i++)
	{
		vector <HuffmanTree::CharBit> :: iterator it;
		for (it = this->char2bit_table.begin(); it != this->char2bit_table.end(); ++it)
		{
			if(text[i] == (*it).data)
				string_bit += (*it).bit;
		}
	}
	this->bit_encoded = string_bit;
}

void HuffmanTree::decode(string bit)
{
	string text;
	MinHeapNode* curr = this->tree_root;
	if(curr == NULL){
		cout << "Chua xay dung cay Huffman" << endl;
		return;
	}
	for(unsigned int i=0; i < bit.size(); i++)
	{
		if(bit[i] == '0')
			curr = curr->left;
		else
			curr = curr->right;
		if (curr->left==NULL && curr->right==NULL)
		{
			text += curr->data;
			curr = this->tree_root;
		}
	}
	this->text_decoded = text;
}

void HuffmanTree::write_to_file()
{
	string file_name;
	char c;
	cout << "Nhap 1 de ghi du lieu text da duoc encode, nhap 2 "
    "de ghi du lieu chuoi bit da duoc decode: ";
	cin >> c;
	if (c != '1' && c != '2')
	{
		cout<<"Nhap sai, quay lai menu truoc" << endl;
		return;
	}
	cout << endl << "Nhap ten file: ";
	cin >> file_name;
	fstream fi;
    if (c == '1')
    {
    	if (this->bit_encoded == ""){
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	fi.open(file_name.c_str(), ios::out);
		fi << this->bit_encoded;
    	fi.close();
        cout << "Da ghi thanh cong" << endl;
    	return;
    }
    else if (c == '2'){
    	if (this->text_decoded == "")
    	{
    		cout << "Chua co du lieu" << endl << endl;
    		return;
    	}
    	fi.open(file_name.c_str(), ios::out);
		fi << this->text_decoded;
        cout << "Da ghi thanh cong" << endl;
    	fi.close();
    	return;
    }
}

vector<HuffmanTree::CharBit> HuffmanTree::get_char2bit_table()
{
        return this->char2bit_table;
}

string HuffmanTree::get_bit_encoded()
{
		return this->bit_encoded;
}

string HuffmanTree::get_text_decoded()
{
    return this->text_decoded;
}

HuffmanTree::MinHeapNode* HuffmanTree::get_tree_root()
{
    return this->tree_root;
}

void HuffmanTree::set_char2bit_table_empty()
{
	this->char2bit_table.clear();
}

void HuffmanTree::set_view_mediate(bool view_mediate){
	if (!view_mediate){
		this->view_mediate = false;
		return;
	}
	else this->view_mediate = true;
	return;
}